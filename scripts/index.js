import { searchTitle } from './elements.js';
import { factory } from './factory.js';
import { Fetch } from './fetch.js';
import { setLastPosts, setTagsFilter } from './htmlControllers.js';

setTagsFilter();

Fetch.GET('/posts?_sort=createDate&_order=asc', factory.createInteraction('render'));
Fetch.GET('/posts?_sort=createDate&_order=asc', setLastPosts);

function searchByTitle(event) {
  event.preventDefault();
  Fetch.GET(`/posts?title_like=${event.target.search.value}`, factory.createInteraction('render'));
}

searchTitle.addEventListener('submit', searchByTitle);
