import {
  postContainer,
  commentContainer,
  selectUsers,
  filtersContainer,
  postsContainer,
  lastPosts,
} from './elements.js';
import { factory } from './factory.js';
import { Fetch } from './fetch.js';
import { commentTemplate, lastPostTemplate, postTemplate } from './htmlTemplates.js';
import { tags as tagObj, users } from './utils.js';

export function renderPosts(posts) {
  postsContainer.innerHTML = '';
  if (posts.length > 0) {
    posts.reverse().forEach((post) => {
      postsContainer.appendChild(postTemplate(post));
    });
  } else {
    postsContainer.appendChild(document.createTextNode('No results found'));
    postsContainer.style.borderRadius = '1rem'
  }
}

export function setPostDetail(response) {
  if(!response.id) return window.location.replace('index.html')
  postContainer.appendChild(postTemplate(response, true));
  window.document.title = response.title;
  postContainer.style.border = 'none';
}

export function setComments(response) {
  if (response.length > 0) {
    response.reverse().forEach((c) => {
      commentContainer.appendChild(commentTemplate(c));
    });
  } else {
    commentContainer.appendChild(document.createTextNode('There are no comments in this post. Be the first one!'));
  }
}

export function setUsers() {
  for (let i = 1; i <= Object.keys(users).length; i++) {
    const option = document.createElement('option');
    option.value = i;
    option.innerHTML = users[i];
    selectUsers.appendChild(option);
  }
}

export function setTagsFilter() {
  for (let i = 1; i <= Object.keys(tagObj).length; i++) {
    const div = document.createElement('div');
    div.className = 'post-tag post-tag-hover';
    div.innerHTML = tagObj[i];
    div.id = i;
    div.onclick = (event) => Fetch.GET('/posts?_sort=createDate&_order=asc', (r) => filterByTag(r, event.target.id));
    filtersContainer.appendChild(div);
  }
}

export function filterByTag(response, tagId) {
  let filteredPosts = response.filter((r) => r.tags.includes(parseInt(tagId)));
  factory.createInteraction('render')(filteredPosts);
}

export function setLastPosts(posts) {
  posts
    .slice(posts.length - 3, posts.length)
    .reverse()
    .forEach((p) => {
      lastPosts.appendChild(lastPostTemplate(p));
    });
}
