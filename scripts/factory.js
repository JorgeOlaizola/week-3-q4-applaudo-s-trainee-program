import { Fetch } from './fetch.js';
import { addLike } from './utils.js';
import { renderPosts, setComments } from './htmlControllers.js';

function deletePost(id) {
  Fetch.DELETE('/posts', id, () => window.location.replace('index.html'));
}

function Factory() {
  this.createInteraction = function (type, opts) {
    let interaction;
    switch (type) {
      case 'render':
        interaction = renderPosts;
        break;
      case 'comment':
        interaction = setComments(opts);
        break;
      case 'delete':
        interaction = deletePost(opts);
        break;
      case 'like':
        interaction = addLike(opts);
      case 'detail':
        interaction = function () {};
        break;
      default:
        console.log('Nothing!');
    }
    return interaction;
  };
}

export const factory = new Factory();
