export function postOpts(bodyContent) {
  return {
    method: 'POST',
    body: JSON.stringify(bodyContent),
    headers: { 'Content-type': 'application/json; charseft=UFT-8' },
  };
}

export function putOpts(bodyContent) {
  return {
    method: 'PUT',
    body: JSON.stringify(bodyContent),
    headers: { 'Content-type': 'application/json; charseft=UFT-8' },
  };
}

export const deleteOpts = {
  method: 'DELETE',
  headers: { 'Content-type': 'application/json; charseft=UFT-8' },
};

export const patchOpts = function (likes) {
  return {
    method: 'PATCH',
    body: JSON.stringify(likes),
    headers: {
      'Content-type': 'application/json',
      Accept: 'application/json',
      'Access-Control-Allow-Origin': '*',
    },
  };
};
