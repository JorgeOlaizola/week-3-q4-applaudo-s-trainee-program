import { deleteOpts } from './fetchOpts.js';
import { baseUrl } from './globals.js';

const Singleton = function () {
  let instance;

  function createInstance() {
    const GET = function (path, cb) {
      fetch(`${baseUrl}${path}`)
        .then((response) => response.json())
        .then((data) => cb(data))
        .catch((error) => console.log(error));
      return;
    };
    const POST = function (path, opts, cb) {
      fetch(`${baseUrl}${path}`, opts)
        .then((response) => response.json())
        .then((data) => (typeof cb === 'function' ? cb(data) : console.log(data)))
        .catch((error) => console.log(error));
      return;
    };
    const DELETE = function (path, id, cb) {
      fetch(`${baseUrl}${path}/${id}`, deleteOpts)
        .then((response) => response.json())
        .then((data) => (typeof cb === 'function' ? cb(data) : console.log(data)))
        .catch((error) => console.log(error));
      return;
    };
    const PUT = function (path, opts, id, cb) {
      fetch(`${baseUrl}${path}/${id}`, opts)
        .then((response) => response.json())
        .then((data) => (typeof cb === 'function' ? cb(data) : console.log(data)))
        .catch((error) => console.log(error));
      return;
    };
    const PATCH = function (path, opts, id) {
      fetch(`${baseUrl}${path}/${id || ''}`, opts)
        .then((response) => response.json())
        .then((data) => console.log(data))
        .catch((error) => console.log(error));
      return;
    };
    return { GET, POST, DELETE, PATCH, PUT };
  }

  return {
    getInstance: function () {
      if (!instance) {
        instance = createInstance();
      }
      return instance;
    },
  };
};

export const Fetch = Singleton().getInstance();
