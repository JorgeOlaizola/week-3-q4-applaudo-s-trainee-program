import { Fetch } from './fetch.js';
import {
  selectAuthors,
  selectTags,
  form,
  titleInput,
  subTitleInput,
  bodyInput,
  imageInput,
  authorsLabel,
  editTitle,
  tagsLabel,
} from './elements.js';
import { authors, dateGenerator, tags } from './utils.js';
import { defaultImg } from './globals.js';
import { postOpts, putOpts } from './fetchOpts.js';

let edit = false;
let cache = {};
let tagsSelected = [];

function setAuthors() {
  for (let i = 1; i <= Object.keys(authors).length; i++) {
    const option = document.createElement('option');
    option.innerHTML = authors[i];
    option.value = i;
    selectAuthors.appendChild(option);
  }
}

function setTagsOptions() {
  for (let i = 1; i <= Object.keys(tags).length; i++) {
    const container = document.createElement('div');
    container.className = 'input-tag';
    const label = document.createElement('label');
    const checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    checkbox.value = i;
    label.innerHTML = tags[i];

    checkbox.addEventListener('change', (event) => {
      if (event.target.checked && !tagsSelected.includes(event.target.value)) tagsSelected.push(event.target.value);
      else {
        tagsSelected = tagsSelected.filter((t) => t !== event.target.value);
      }
    });

    container.appendChild(label);
    container.appendChild(checkbox);
    selectTags.appendChild(container);
  }
}

function onSubmit(event) {
  event.preventDefault();

  if (!edit) {
    const { title, subTitle, body, image, selectAuthors, selectTags } = event.target;
    Fetch.POST(
      '/posts',
      postOpts({
        title: title.value,
        subTitle: subTitle.value,
        body: body.value,
        image: image.value || defaultImg,
        author: selectAuthors.value,
        tags: tagsSelected,
        likes: 0,
        createDate: dateGenerator(),
      }),
      () => window.location.replace('index.html')
    );
  } else {
    const { title, subTitle, body, image } = event.target;
    cache.title = title.value;
    cache.subTitle = subTitle.value;
    cache.body = body.value;
    cache.image = image.value;
    Fetch.PUT(`/posts`, putOpts(cache), cache.id, () => window.location.replace(`/details.html?post=${cache.id}`));
  }
}

function setEditForm(response) {
  if (!response.title) return window.location.replace('index.html');
  edit = true;
  cache = response;

  const { title, subTitle, body, image, author } = response;
  titleInput.value = title;
  subTitleInput.value = subTitle;
  bodyInput.value = body;
  imageInput.value = image;

  editTitle.appendChild(document.createTextNode(`Edit "${title}" by ${authors[author]}`));
  editTitle.style.marginTop = '2rem';
  selectAuthors.style.display = 'none';
  authorsLabel.style.display = 'none';
  selectTags.style.display = 'none';
  tagsLabel.style.display = 'none';

  window.document.title = `Edit "${title}"`;
}

setTagsOptions();
setAuthors();

if (window.location.search) {
  const url = window.location.search.split('=');
  if (url[0] === '?edit' && url[1]) {
    let id = url[1];
    Fetch.GET(`/posts/${id}`, setEditForm);
  }
}

form.addEventListener('submit', (e) => onSubmit(e));
