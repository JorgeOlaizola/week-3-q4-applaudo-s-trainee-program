import { factory } from './factory.js';
import { tags as tagObj, authors, authorsPictures, usersPictures, users } from './utils.js';

let cache = {};

function postTemplateTags(tag) {
  const tagItem = document.createElement('div');
  tagItem.className = 'post-tag';
  tagItem.innerHTML = tagObj[tag];
  return tagItem;
}

export function postTemplate(post, detail) {
  const { id, author, createDate, title, subTitle, body, image, tags, likes } = post;
  cache[id] = post;

  const container = document.createElement('div');
  const containerAuthorDate = document.createElement('div');
  const authorCont = document.createElement('div');
  const authorPicture = document.createElement('img');
  const authorName = document.createElement('div');
  const dateCont = document.createElement('div');
  const titleCont = document.createElement('h3');
  const subTitleCont = document.createElement('h5');
  const bodyCont = document.createElement('p');
  const imageCont = document.createElement('img');
  const tagsCont = document.createElement('div');
  const likesCont = document.createElement('div');
  const postButtonsCont = document.createElement('div');
  const deleteButton = document.createElement('button');
  const editButton = document.createElement('button');
  const likeButton = document.createElement('button');

  container.id = id;
  container.className = 'post';

  authorName.innerHTML = authors[author];
  authorName.className = 'post-author';

  authorPicture.src = authorsPictures[author];
  authorPicture.className = 'post-author-picture';

  authorCont.appendChild(authorPicture);
  authorCont.appendChild(authorName);
  authorCont.className = 'post-author-container';

  containerAuthorDate.appendChild(authorCont);

  dateCont.innerHTML = createDate.split('/').reverse().join('/');
  dateCont.className = 'post-date';

  containerAuthorDate.appendChild(dateCont);
  containerAuthorDate.className = 'post-author-date-container';

  container.appendChild(containerAuthorDate);

  titleCont.innerHTML = title;
  titleCont.className = 'post-title';

  container.appendChild(titleCont);

  subTitleCont.innerHTML = subTitle;
  subTitleCont.className = 'post-subtitle';

  container.appendChild(subTitleCont);

  bodyCont.innerHTML = body;
  bodyCont.className = 'post-body';

  container.appendChild(bodyCont);

  imageCont.src = image;
  imageCont.className = 'post-img';

  container.appendChild(imageCont);

  tags.forEach((t) => {
    tagsCont.appendChild(postTemplateTags(t));
  });
  tagsCont.className = 'post-tags';

  container.appendChild(tagsCont);

  likesCont.innerHTML = `${likes} people love this post!`;

  container.appendChild(likesCont);

  postButtonsCont.className = 'post-buttons';

  deleteButton.className = 'post-button post-button-delete';
  deleteButton.onclick = () => factory.createInteraction('delete', id);
  deleteButton.innerHTML = '❌ Delete';

  editButton.className = 'post-button post-button-edit';
  editButton.onclick = () => window.location.replace(`create.html?edit=${id}`);
  editButton.innerHTML = '✍🏼 Edit';

  likeButton.className = 'post-button post-button-like';
  likeButton.onclick = () => factory.createInteraction('like', { post: cache[id], id });
  likeButton.innerHTML = '💖 Like';

  if (!detail) {
    const detailButton = document.createElement('button');
    detailButton.className = 'post-button post-button-detail';
    detailButton.onclick = () => window.location.replace(`details.html?post=${id}`);
    detailButton.innerHTML = '🌐 Detail';
    postButtonsCont.appendChild(detailButton);
  }

  postButtonsCont.appendChild(deleteButton);
  postButtonsCont.appendChild(editButton);
  postButtonsCont.appendChild(likeButton);

  container.appendChild(postButtonsCont);

  return container;
}

export function lastPostTemplate(lastPost) {
  const { author, id, title } = lastPost;

  const container = document.createElement('div');
  const titleContent = document.createElement('div');
  const authorContent = document.createElement('div');
  container.className = 'features-last-posts-item';
  authorContent.innerHTML = authors[author];
  authorContent.className = 'features-last-posts-author';
  titleContent.innerHTML = title;
  titleContent.className = 'features-last-posts-title';
  container.onclick = () => {
    window.location.replace(`details.html?post=${id}`);
  };
  container.appendChild(titleContent);
  container.appendChild(authorContent);

  return container;
}

export function commentTemplate(commentInfo) {
  const { comment, user } = commentInfo;

  const container = document.createElement('div');
  container.className = 'post-comment';

  const usercont = document.createElement('div');
  usercont.className = 'post-comment-user-container';

  const userPicture = document.createElement('img');
  userPicture.className = 'post-comment-user-picture';
  userPicture.src = usersPictures[user];

  usercont.appendChild(userPicture);

  const username = document.createElement('div');
  username.className = 'post-comment-user';
  username.innerHTML = users[user];

  usercont.appendChild(username);

  const body = document.createElement('div');
  body.className = 'post-comment-body';
  body.innerHTML = comment;

  container.appendChild(usercont);
  container.appendChild(body);

  return container;
}

// <div class="post" id="${id}">
// <div class="post-author">${authors[author]}</div>
// <div class="post-date">${createDate.split('/').reverse().join('/')}</div>
// <h3 class="post-title">${title}</h3>
// <h5 class="post-subtitle">${subTitle}</h5>
// <p class="post-body">${body}</p>
// <img class="post-img" src="${image}"/>
// <div class="post-tags">
//   ${tags.length > 0 && tags.map((t) => {
//         return `<div class="post-tag">${tagObj[t]}</div>`;
//     }).join('')
//   }
// </div>
// <div>Likes: ${likes}</div>
// <div class="post-buttons" id=post-buttons-${id}>
// <button class="post-delete-button" id="delete-${id}">Delete</button>
// <button id="edit-${id}">Edit</button>
// <button>Details</button>
// <button class="post-like-button" id="like-post-${id}">Like</button>
// </div>
// </div>`
