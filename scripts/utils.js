import { Fetch } from './fetch.js';
import { putOpts } from './fetchOpts.js';

export const tags = {
  1: 'Animals',
  2: 'Nature',
  3: 'Food',
  4: 'Life style',
  5: 'Travel',
  6: 'Sports',
};

export const authors = {
  1: 'John Wick',
  2: 'John Cena',
  3: 'Bruce Wayne',
};

export const authorsPictures = {
  1: 'https://www.rockandpop.cl/wp-content/uploads/2019/07/john-wick.jpg',
  2: 'https://www.biography.com/.image/t_share/MTIwNjA4NjMzNzAzNzI4NjUy/john-cena-562300-1-402.jpg',
  3: 'https://img.europapress.es/fotoweb/fotonoticia_20160219105422_1024.jpg',
};

export const users = {
  1: 'Anakin Skywalker',
  2: 'Padme Amidala',
  3: 'Obi-Wan Kenobi',
};

export const usersPictures = {
  1: 'https://th.bing.com/th/id/R.5ce4ded9dce4b497de23261497d4afe1?rik=oUdxAIfQNMX0Gw&pid=ImgRaw&r=0',
  2: 'https://th.bing.com/th/id/OIP.4SeftkXbc2Blss3J3ppoAAHaKW?pid=ImgDet&rs=1',
  3: 'https://th.bing.com/th/id/OIP.0FgReHOYxmCCvhpYXLMPBwHaEJ?pid=ImgDet&rs=1',
};
export function addLike({ post, id }) {
  post.likes++;
  Fetch.PUT(`/posts`, putOpts(post), id, () => {
    window.location.replace(`details.html?post=${id}`);
  });
}

export function dateGenerator() {
  let date = new Date();
  let dateString = `${date.getFullYear()}/${date.getMonth() + 1}/${date.getDate()}`;
  return dateString;
}
