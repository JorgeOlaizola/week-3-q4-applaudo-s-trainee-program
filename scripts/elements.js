// INDEX Elements

export const postsContainer = document.getElementById('posts-container');
export const deleteButtons = document.getElementsByClassName('post-delete-button');
export const filtersContainer = document.getElementById('filters-container');
export const searchTitle = document.getElementById('searchTitle');
export const lastPosts = document.getElementById('last-posts');

// FORM Elements

export const editTitle = document.getElementById('edit-title');
export const selectAuthors = document.getElementById('selectAuthors');
export const authorsLabel = document.getElementById('authorsLabel');
export const selectTags = document.getElementById('selectTags');
export const form = document.getElementById('form');
export const titleInput = document.getElementById('title');
export const subTitleInput = document.getElementById('subTitle');
export const bodyInput = document.getElementById('body');
export const imageInput = document.getElementById('image');
export const tagsLabel = document.getElementById('tagsLabel')

// DETAIL Elements

export const postContainer = document.getElementsByClassName('post')[0];
export const commentContainer = document.getElementsByClassName('post-comment-container')[0];
export const commentForm = document.getElementsByClassName('post-comment-form')[0];
export const selectUsers = document.getElementById('users');
