import { Fetch } from './fetch.js';
import { setPostDetail, setUsers } from './htmlControllers.js';
import { commentForm } from './elements.js';
import { factory } from './factory.js';
import { postOpts } from './fetchOpts.js';

setUsers();

if (window.location.search) {
  const query = window.location.search.split('=');
  if (query[0] === '?post' && query[1]) {
    Fetch.GET(`/posts/${query[1]}`, setPostDetail);
    Fetch.GET(`/comments?postId=${query[1]}`, (data) => factory.createInteraction('comment', data));
  } else console.log('Invalid');
}

function submitComment(event) {
  event.preventDefault();
  const postId = window.location.search.split('=')[1];
  const { user, comment } = event.target;
  const opts = {
    postId: parseInt(postId),
    user: parseInt(user.value),
    comment: comment.value,
  };
  Fetch.POST('/comments', postOpts(opts), () => {
    window.location.reload();
  });
}

commentForm.addEventListener('submit', submitComment);
