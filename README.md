
# Twatter Application - Week 3 Q4 Applaudo Trainee Program

Application that uses the JSON-Server api (https://github.com/typicode/json-server) that allows you to create, edit, delete and look for posts in a mock social media page.

## Try it

Link to deploy: https://week-3-q4-applaudo-s-trainee-program.vercel.app/

## Run Locally

Clone the project

```bash
  https://gitlab.com/JorgeOlaizola/week-3-q4-applaudo-s-trainee-program.git
```

Once the project it´s cloned, install dependencies

```bash
  npm install
```

In /scripts/global.js change the const baseUrl to 'http://localhost:3000'

```javascript
  export const baseUrl = 'http://localhost:3000'
```

Before you run the project with Live Server, execute the following command for 
start the server.

```bash
  json-server --watch db.json
```
